# Objetivos

Las personas participantes conocerán los principios de la edición de video no lineal
por medio de programas de edición con licencias libres para generar materiales
audiovisuales sencillos.

# Primer tema

## Video

* Qué es el video
* Formatos y codecs
* Contenedores
* Salida de video HD, Full HD, 4k, 8k.

## Audio

* Qué es el audio
* Formatos y codecs
* Salida de audio

# Segundo tema

* Editores de video basados en Software Libre
* Framework MLT
* La elección Shotcut
* Instalación en GNU/Linux
* Instalación en Windows
* ¿Instalación en Mac?

**Actividad** Instalar el programa

* Editores de audio basados en Software Libre
* La elección Audacity
* Instalación en GNU/Linux
* Instalación en Windows
* ¿Instalación en Mac?

**Actividad:** Instalar el programa

# Tercer tema

* Licencias libres para obras intelectuales
  * Copy Left Creative Commons
  * Copy Far Left

* Fuentes de imagen audio y video con licencias libres:
    * https://creativecommons.org/
    * https://archive.org/
    * pixabay.com/es/
    * https://freemusicarchive.org

**Actividad 1:** Piensa y describe un material audiovisual.

Considera lo siguiente:

* Cuál es el tema
* A quién va dirigido
* Para qué es
* Cuál es su duración
* Qué sonidos debería tener
* Qué imágenes debería tener

**Actividad 2:** Consigue imágenes, clips de video y sonidos con licencia Creative Commons
que refuercen el material audiovisual que describiste en la actividad anterior.

# Cuarto tema

* Conoce la interfase de Shotcut
* Línea de tiempo
* Separación de Audio y video
* Preselección
* Recortar escenas
* Transiciones
* Recortar fuente
* Rotar y escalar
* Tamaño y posición
* cámara rápida y lenta

# Quinto tema

* Fotogramas clave
* Superposición de capas
* Capas de audio Sonido ambiente, Voces, Música
* Capas de video
  * Video principal
  * Cortinillas
  * Subtítulos
  * Textos auxiliares

# Sexto tema

* Subtítulos (Aegisub y Handbrake)
* Reducción de ruido (Audacity)
* Amplificar (Audacity)
* Limitar (Audacity)
* Comprimir

# Séptimo tema

* Renderización
* Publicación
