# Presentación

%%%D1

Video digital

Es una representación de imágenes en movimiento en la forma de datos digitales
codificados [...]

Los formatos de video digital de la actualidad abarcan desde los formatos sin
compresión hasta los formatos de video comprimidos populares como H.264 y MPEG-4.

Es un Sistema de grabación y reproducción de imágenes, con o sin sonidos.
También podemos considerar el vídeo como el conjunto de fotogramas que se
reproducen a gran velocidad provocando la sensación de movimiento.
Los fotogramas, son todas las imágenes que de manera aislada se suceden y
conforman la película y para que se perciba efectivamente el efecto de
movimiento, los fotogramas se deben mostrar a cierta velocidad.

* Codecs
https://www.wikiversus.com/informatica/codec-vs-contenedor/
* Contenedores


    Formatos y codecs

    Contenedores

    Salida de video HD, Full HD, 4k, 8k.


%%%
